console.log("ES6 Updates!");

// ES6 Updates
/*
    ES6 - One of the latest versions of writing JS; one of the major updates
  
    In JS, hoisting allows for the use of functions & variables before declaration
    HOWEVER, this may cause confusion - it is best to avoid using functions before declaration

*/

// Problems associated with using var
console.log(varSample); // undefined
var varSample = "Hoist me up!";

var nameVar = "Hachi";

if (true) {
  var nameVar = "Hi";
}

var nameVar = "C";

console.log(nameVar);

let name1 = "Cee";

if (true) {
  let name1 = "Hello!";
}

// let name1 = "Hello World";

console.log(name1);

// ** Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

let string1 = "fun";
let string2 = "Bootcamp";
let string3 = "coding";
let string4 = "JavaScript";
let string5 = "Zuitt";
let string6 = "love";
let string7 = "learning";
let string8 = "I";
let string9 = "is";
let string10 = "in";

// Mini-Activity
/* 
    Create a variable called concatSentence1
    Concatenate and save a resulting string into concatSentence1
    Log concatSentence1 in your console
*/

let concatSentence1 = `${string8} ${string6} ${string3} ${string10} ${string5} ${string2}. ${string7} ${string4} ${string9} ${string1}.`;
console.log(concatSentence1);

// Template Literals
// Allows us to write strings without using concatenation operator (+)

let concatSentence2 = `${string8} ${string6} ${string5} ${string3} ${string2}`;
console.log(concatSentence2);

// ${} = placeholder for embedding JS expressions when creating strings

let name = "Makoto";

let message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

const anotherMessage = `

${name} attended a math competition.

He won it by solving the problem 8**2 with the solution of ${firstNum}!
    
    `;

console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;
console.log(
  `The interest on your savings account is: ${principal * interestRate}`
);

let dev = {
  name: "Peter",
  lastName: "Parker",
  occupation: "Web Developer",
  income: 50000,
  expenses: 60000,
};

console.log(`${dev.name} is a ${dev.occupation}`);

console.log(
  `His income is ${dev.income}, and his expenses are at ${
    dev.expenses
  }. His current balance is ${dev.income - dev.expenses}`
);

// Array Destructuring
/*
  Allows us to unpack elements in arrays to distinct variables
  Allows us to name array elements with variables instead of index numbers
  Helps with code readability

  Syntax:
    let/const [variableName1, variableName2, variableName3] = array;
*/

const fullName = ["Juan", "dela", "Cruz"];
// Pre-Array Destructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(
  `Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`
);

// Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(
  `Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`
);

let fruits = ["Mango", "Grapes", "Guava", "Apple"];
let [fruit1, , fruit3] = fruits;
console.log(fruit1);
console.log(fruit3);

let kupunanNiEugene = [
  "Eugene",
  "Alfred",
  "Vincent",
  "Dennis",
  "Toguro",
  "Master Jeremiah",
];

let [yusuke, kuwabara, hiei, kurama, , genkai] = kupunanNiEugene;
console.log(`${yusuke} ${kuwabara} ${hiei} ${kurama} ${genkai}`);

// Object Destructuring
/* 
    Allows us to unpack properties of objects into distinct variables
    Shortens the syntax for accessing properties from objects

    Syntax:
        let/const {}
*/

const person = {
  givenName: "Jane",
  maidenName: "dela",
  familyName: "Cruz",
};

// Pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(
  `Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`
);

// Object Destructuring
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.group(familyName);

console.log(
  `Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you again!`
);

// Unlike array destructuring, the specific property name cannot be renamed/changed.

function getFullName({ givenNames, maidenNames, familyNames }) {
  console.log(`${givenName}, ${maidenName}, ${familyName}`);
}

getFullName(person);

// Arrow Functions
/* 
    Compact alternative syntax to traditional functions
    Useful for code snippets where created functions will not be reused in any other portion of the code
    Adhere to "DRY" (Don't Repeat Yourself) principle
*/

function displayMsg() {
  console.log("Hi");
}

displayMsg();

let displayHello = () => {
  console.log("Hello World!");
};

displayHello();

/*
function printFullName(firstName, middleInitial, lastName) {
  console.log(`${firstName} ${middleInitial} ${lastName}`);
}
*/

let printFullName = (firstName, middleInitial, lastName) => {
  console.log(`${firstName} ${middleInitial} ${lastName}`);
};

printFullName("John", "D", "Smith");

const students = ["John", "Jane", "Natalia", "Jobert", "Joe"];

// Arrow functions with loops

// Pre-arrow
students.forEach(function (student) {
  console.log(`${student} is a student.`);
});

students.forEach((student) => {
  console.log(`(Arrow Function) ${student} is a student!`);
});

// Implicit Return Statement
/* 
    There are instances when you can omit the "return" statement
    JS implicitly adds return to the result of the function
*/

/* 
function add(x, y) {
  return x + y;
}

let total = add(1, 2);
console.log(total);
*/

const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);

// Default Function Argument Value

// Default value declared for "name"
const greet = (name = "User") => {
  return `Good morning, ${name}!`;
};

console.log(greet());
console.log(greet("John"));

// Class-Based Object Blueprints
// Allows the creation/instantiation of objects using classes as blueprints

// Creating a Class
/*
    The constructor is a special method of a class for creating/initializing an object for that class
    "This" - refers to properties of an object created/initialized from the class
*/

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

// new Car("Lexus", GX, "2024");

let myCar = new Car();

console.log(myCar);

myCar.brand = "Lexus";
myCar.name = "GX";
myCar.year = 2024;

console.log(myCar);

const myNewCar = new Car("Porsche", "Cayenne", 2024);
console.log(myNewCar);

// Traditional Functions vs Arrow Function as Methods

let character1 = {
  name: "Cloud Strife",
  occupation: "Soldier",
  greet: () => {
    // In a traditional function: the "this" keyword refers to the current object where the method is
    console.log(this); // Displays "global window object" - container of browser's global variables, functions, and objects
    console.log(`Hi! I'm ${this.name}.`);
  },
  // In this scenario, the arrow function does not work as intended
  introduceJob: function () {
    console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}.`);
  },
};

character1.greet();
character1.introduceJob();

// Mini-Activity
// Create a "character" class contructor

class Character {
  constructor(name, role, strength, weakness) {
    this.name = name;
    this.role = role;
    this.strength = strength;
    this.weakness = weakness;
    // Arrow methods can be used within a constructor
    this.introduce = () => {
      console.log(`Hi! I'm ${this.name}. I'm a ${this.role}.`);
    };
  }
}

let myNewCharacter = new Character(
  "Gojou Satoru",
  "Jujutsushi",
  "Mukagen",
  "Getou Suguru"
);

console.log(myNewCharacter);
myNewCharacter.introduce();
