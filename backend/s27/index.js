console.log("Array Manipulation *:･ﾟ✧*:･ﾟ✧");

// Array Methods
// Built-in functions & methods that allow us to manipulate and access array items

// Mutator Methods
/*
    - Functions that mutate or change an array after they are created
    - Manipulate the originl array performing tasks such as adding & removing elements
*/

// Array
let fruits = ["Apple", "Orange", "Kiwi", "Dragonfruit"];
console.log(fruits);
console.log(typeof fruits);

// Push()
// Adds an element at the end of an array & returns the array's length
// Syntax: arrayName.push("itemToPush");

console.log("Current Array");
console.log(fruits);

// If push is assigned to a variable, we are able to save the length of the array
let fruitsLength = fruits.push("Mango");
console.log("Mutated Array (Push):");
console.log(fruitsLength);
console.log(fruits);

/*
Multiple items can be pushed at the same time

fruits.push("Strawberry", "Blueberry");
console.log(fruits);
*/

// Pop()
// Removes last element in an array
// Syntax: arrayName.pop();

let removedFruit = fruits.pop();
console.log(`Removed Fruit: ${removedFruit}`);
console.log("Mutated Array (Pop):");
console.log(fruits);

// Mini-Activity
/*
    Create a function, "removeFruit"
    Deletes the last item in the array
    After invoking removeFruit, log the fruits array in the console
*/

function removeFruit(arrayName) {
  arrayName.pop();
}

removeFruit(fruits);
console.log(fruits);

// Unshift()
// Adds one or more elements at the beginning of an array
/* 
    Syntax:
    arrayName.unshift("element")
    arrayName.unshift("elementA", "elementB")
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated Array (Unshift):");
console.log(fruits);

function unshiftFruit(fruit1, fruit2) {
  fruits.unshift(fruit1, fruit2);
}

unshiftFruit("Strawberry", "Blueberry");
console.log(fruits);

// Shift()
// Deletes an element at the beginning of an array and returns the removed element
// Syntax: arrayName.shift();

let anotherFruit = fruits.shift();
console.log(`Shifted Fruit: ${anotherFruit}`);
console.log("Mutated Array (Shift):");
console.log(fruits);

// Splice();
// Simultaneously removes elements from a specified index number and adds elements
// Syntax: arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

fruits.splice(1, 2, "Calamansi", "Cherry");
console.log("Mutated Array (Splice):");
console.log(fruits);

function spliceFruit(index, deleteCount, fruit) {
  fruits.splice(index, deleteCount, fruit);
}

spliceFruit(1, 1, "Avocado");
spliceFruit(2, 1, "Durian");
// spliceFruit(2, 1) - results in undefined
// spliceFruit(2) - also results in undefined
console.log(fruits);

// Sort()
// Rearranges the array elements in alphanumeric order
// Syntax: arrayName.sort();

fruits.sort();
console.log("Mutated Array (Sort):");
console.log(fruits);

// Reverse()
// Reverses the order of array elements (reverse alphanumeric order)
// Syntax: arrayName.reverse();

fruits.reverse();
console.log("Mutated Array (Reverse):");
console.log(fruits);

// ACTIVITY

/*
    
   1. Create a function called addPokemon which will allow us to register/add new pokemon into the registeredPokemon list.
        - this function should be able to receive a string.
        - add the received pokemon into the registeredPokemon array.
*/

let registeredPokemon = [];
let registeredTrainers = [];

function addPokemon(pokemon) {
  registeredPokemon.push(pokemon);
  console.log(`Added ${pokemon} to your PokeDex!`);
}

addPokemon("Pikachu");
console.log(registeredPokemon);

function deletePokemon() {
  if (registeredPokemon.length > 0) {
    registeredPokemon.pop();
  } else {
    return "No registered pokemon.";
  }
}

deletePokemon();
console.log(registeredPokemon);

function sortPokemon() {
  if (registeredPokemon.length === 0) {
    return "No pokemon registered.";
  } else {
    return registeredPokemon.sort();
  }
}

function registerTrainer(name, level, pokemon) {
  let trainer = {
    trainerName: name,
    trainerLevel: level,
    pokemon: pokemon,
  };
  registeredTrainers.push(trainer);
}
