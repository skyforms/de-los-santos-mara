// JavaScript - Synchronouus vs Asynchronous
/* 
    JS, by default, is synchronous - only one statement is executed at the time
*/

// console.log("Hey!");
// cosnole.log("Hello from cosnole!");
// console.log("I'll run if no error occurs!");

/* 
console.log("Hi before the loop!");

for (let i = 0; i <= 50; i++) {}

console.log("Hi after the loop!");
*/

// Asynchronous Javascript
// It is possible to proceed to execute other statements while time consuming code is running in the background

// [Getting All Posts]
// The Fetch API allows us to asynchronously request a resource or data
// A "Promise" is an OBJECT that represents the eventual completion (or failure) of an asynchronous function, and its resulting value
// Promises are special containers for a future value that may not be available immediately. The promise represents the outcome of an asynch operation that may take some time to complete (such as fetching data from a server, reading a file, getting data from the data, and other tasks that do not happen instantly)

/* 
    Syntax:
    fetch("URL")
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// Fetch() returns a Promise which can be chained using .then()
// Then() waits for the promise to be resolved before executing the code

/* 
    Syntax:
    fetch("URL")
    .then((response)=>{})
*/

fetch("https://jsonplaceholder.typicode.com/posts").then((response) =>
  console.log(response.status)
);

fetch("https://jsonplaceholder.typicode.com/posts")
  // Use the "JSON" method from the response object to convert the data retrieved into JSON formatting (not a string) to be used in the application
  .then((response) => response.json())
  // Log the converted JSON value from the "fetch()" request
  // When there are multiple .then methods, a promise chain is created
  .then((posts) => console.log(posts));

async function fetchData() {
  //Waits for the "fetch()" method to finish, then stores the value in the "result" variable
  let result = await fetch("https://jsonplaceholder.typicode.com/posts");
  console.log(result);
  console.log(typeof result);
  console.log(result.body);

  // Converts the data from the response to JSON
  let json = await result.json();
  console.log(json);
}

fetchData();

// [Retrieves a Specific Post]
fetch("https://jsonplaceholder.typicode.com/posts/18")
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Creating a Post]

fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "New post",
    body: "Hello world!",
    userID: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Updating a Specific Post]
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-type": "application/json",
  },
  body: JSON.stringify({
    title: "Corrected post",
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Deleting a Post]

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
});

// [Filtering Posts]
/* 
    Syntax:
        - Individual parameters
            'url?parameterName=value'
        - Multiple parameters
            'url?paramA=valueA&paramB=valueB'

*/

fetch("https://jsonplaceholder.typicode.com/posts?userID=1")
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts?userID=1&id=6")
  .then((response) => response.json())
  .then((json) => console.log(json));

// [Retrieve Nested/Related Comments to Posts]

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((response) => response.json)
  .then((json) => console.log(json));

async function updateToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Updated To Do List Item",
      description: "To update my to do list with a different data structure.",
      status: "Pending",
      dateCompleted: "Pending",
      userID: 1,
    }),
  })
    .then((response) => response.json())
    .then((json) => json);
}

async function createToDo() {
  return await //Add fetch here.

  fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Created a To Do List Item",
      completed: false,
      userId: 1,
    }),
  })
    .then((response) => response.json())
    .then((json) => json);
}
