console.log("Array Non-Mutator and Iterator Methods");

/* 

Non Mutator Methods
    Methods that do not modify or change an array after they are created
    Do not manipulate the original array, performing tasks such as:
        Returning elements from an array
        Combining arrays
        Printing output 
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()
/* 
    Returns the index number of the first matching element found in an array
    If no match is found, the result will be -1 
*/

let firstIndex = countries.indexOf("PH");
console.log(`Result of indexOf Method: ${firstIndex}`);

let invalidCountry = countries.indexOf("BR");
console.log(`Result of indexOf Method: ${invalidCountry}`);

// lastIndexOf()
// Returns the index number of the last matching element found in an array

let lastIndex = countries.lastIndexOf("PH");
console.log(`Result of lastIndexOf Method: ${lastIndex}`);

let lastIndexStart = countries.lastIndexOf("PH", 6);
console.log(`Result of lastIndexOf Method: ${lastIndexStart}`);

// slice()
// Portions/slices elements from an array and returns a new array

console.log(countries);
// [ "US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE" ]

let slicedArrayA = countries.slice(2);
console.log("Result from Slice Method:");
console.log(slicedArrayA);
// [ "CAN", "SG", "TH", "PH", "FR", "DE" ]

let slicedArrayB = countries.slice(2, 4);
console.log("Result from Slice Method:");
console.log(slicedArrayB);
// [ "CAN", "SG" ]

let slicedArrayC = countries.slice(-3);
console.log("Result from Slice Method:");
console.log(slicedArrayC);
// [ "PH", "FR", "DE" ]

// toString()
// Returns an array as a string separated by commas

let stringArray = countries.toString();
console.log("Result from toString Method");
console.log(stringArray);

let sampArr = [1, 2, 3];
console.log(sampArr.toString());

// concat()
// Combines two arrays and returns the combined result

let taskArrayA = ["Drink HTML", "Eat JavaScript"];
let taskArrayB = ["Inhale CSS", "Breathe MongoDB"];
let taskArrayC = ["Get Git", "Be Node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat Method");
console.log(tasks);

console.log("Result from concat Method");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log("Result from concat Method");

console.log(allTasks);

let combinedTasks = taskArrayA.concat("Smell Express", "Throw React");
console.log("Result from concat Method");
console.log(combinedTasks);

// join()
// Returns an array as a string separated by specified separator (string)

let users = ["Makoto", "Haru", "Rei", "Nagisa"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));

/*
    Iteration Methods
    Loops designed to perform repetitive tasks on arrays
    Array Iteration methods normally work with a function supplied as an argument
*/

// forEach()
/* 
    Similar to a for loop that iterates on each array element
    For each item in the array, the anonymous function passed in the forEach() method will be run
    The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
    Variable names for arrays are normally written in the plural form
    Common practice to use singular form of the array content for the parameter names used in array loops
    Does NOT return anything
*/

console.log(allTasks);
allTasks.forEach(function (task) {
  console.log(task);
});

let filteredTasks = [];

allTasks.forEach(function (task) {
  // Selection Control Structure Review: if, else, else if, switch
  if (task.length > 10) {
    filteredTasks.push(task);
  }
});

console.log("Result of filteredTasks:");
console.log(filteredTasks);
// ['Eat JavaScript', 'Breathe MongoDB']

// map()
/* 
    map() vs forEach() - map requires the use of return
    Iterates each element AND returns new array with different values depending on the result of the function's operation
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function (number) {
  return number * number;
});

console.log("Original Array");
console.log(numbers);
console.log("Result of Map Method");
console.log(numberMap);

// map() vs forEach()

let numberForEach = numbers.forEach(function (number) {
  return number * number;
});

console.log(numberForEach);

// every()
/* 
    Checks if all elements in an array meet a given condition
    Returns a boolean value
*/

let allValid = numbers.every(function (number) {
  return number < 3;
});

console.log("Result of every Method:");
// Are all numbers in the array less than 3?
console.log(allValid); // False

// some()
// Checks if at least one element in the array meetss the given condition

let someValid = numbers.some(function (number) {
  return number < 2;
});

console.log("Result of some Method");
//Are some numbers in the array less than 2?
console.log(someValid); // True

// filter()
/* 
    Returns a new array that containes elements which meets the given condition
    Returns an empty array if no elements were found
*/

let filterValid = numbers.filter(function (number) {
  return number < 3;
});

console.log("Result of Filter Method");
// Which numbers are less than 3?
console.log(filterValid); // 1, 2

// Mini-Activity

let filteredNumbers = [];

numbers.forEach(function (number) {
  if (number < 3) {
    filteredNumbers.push(number);
  }
});

console.log("Result of forEach Method:");
console.log(filteredNumbers);

// includes()
// Checks if the argument passed can be found in the array

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// Methods can be "chained" by using them one after another

let filteredProducts = products.filter(function (product) {
  return product.toLowerCase().includes("a");
});

console.log(filteredProducts);

// reduce()
/*
    Evaluates elements form left to right
    Returns/reduces the array into a single value

    "Accumulator" parameter in the function stores the result for every iteration of the loop
    "Current value" parameter is the current element in the array evaluated in each iteration of the loop

    Process:
    1. First element in the array is stored in the "accumulator"
    2. Second element in the array is stored in the "current value"
    3. An operation is performed on the 2 elements
    4. The loop repeats the whole step 1-3 until all elements have been worked on
*/

console.log(numbers); // [1, 2, 3, 4, 5]
// First accumulator: 1
// 1 + 2 = 3
// 3 = new accumulator
// 3 + 3 (next element in array) = 6
// 6 = new accumulator
// 6 + 4 = 10
// 10 + 5 = 15

let iteration = 0;

let reducedArray = numbers.reduce(function (x, y) {
  console.warn(`Current iteration: ${++iteration}`);
  console.log(`Accumulator: ${x}`);
  console.log(`Current Value: ${y}`);
  return x + y;
});

console.log(`Result of Reduce Method: ${reducedArray}`);

// Reduce string arrays to string
let list = ["Hello", "from", "the", "other", "side"];
let reducedJoin = list.reduce(function (x, y) {
  console.log(x);
  console.log(y);
  return x + " " + y;
});

console.log(`Result of Reduce Method: ${reducedJoin}`);

// Activity
/*

   1. Create a function called displayValues() which is able to receive an array of numbers and display cubed values in the console.
        - Use the forEach() method to print the square of each number on a new line.

*/

function displayValues(array) {
  array.forEach(function (number) {
    console.log(number * number * number);
  });
}

function celsiusToFahrenheit(temps) {
  let convertedArray = temps.map(function (celsius) {
    return (celsius * 9) / 5 + 32;
  });
  return convertedArray;
}
