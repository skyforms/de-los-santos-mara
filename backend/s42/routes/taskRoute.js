// Application Endpoints

const express = require("express");

// Create a router instance that functions as a middleware and routing system

const router = express.Router();

// Use functions inside taskController.js
const taskController = require("../controllers/taskController");
const task = require("../models/task");

// [Routes]
// Responsible for defining the URIs that our client accesses, and the corresponding controller functions that to be used when a route is accessed
// All the business logic is done in the controller

router.get("/", (req, res) => {
  taskController
    .getAllTasks()
    .then((resultFromController) => res.send(resultFromController));
});

router.post("/", (req, res) => {
  taskController
    .createTask(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

/* 
    Route - Deleting a Task
    - Expects to receive a DELETE request at the url "/tasks/:id"
    - http://localhost:4000/tasks/:id
    - Task ID is obtained from the URL and denoted by ":id"
*/

router.delete("/:id", (req, res) => {
  taskController
    .deleteTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

/* 
    Route - Updating a Task
    - Expects to receive a PUT request at the url "/tasks/:id"
*/

router.put("/:id", (req, res) => {
  taskController
    .updateTask(req.params.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// ACTIVITY
// GET a Specific Task
router.get("/:id", (req, res) => {
  taskController
    .getSpecificTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

// Change task status to complete
router.put("/:id/complete", (req, res) => {
  taskController
    .completeTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

// Use module.exports to export the router object to use in index.js
module.exports = router;
