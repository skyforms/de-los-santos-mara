// Controllers contain the functions and business logic of our Express JS application
const Task = require("../models/task");

module.exports.getAllTasks = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};

module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name,
  });
  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return task;
    }
  });
};

// Controller function for deleting a task
// "taskID" - URL parameter passed from "taskRoute.js" file
/*
        Business Logic
        - Look for the task with the corresponding ID provided in the URL/route
        - Delete the task using the Mongoose method "findByIDAndRemove" with the same ID provided in the route
    */

module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return removedTask;
    }
  });
};

// Controller function for updating a task
/* 
    Business Logic
    - Get the task with its ID using the Mongoose method "findById"
    - Replace the task's name returned from the database with the "name" property from the request body
    - Save task
*/

module.exports.updateTask = (taskId, newContent) => {
  return Task.findByIdAndUpdate(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    }
    result.name = newContent.name;

    return result.save().then((updatedTask, saveErr) => {
      if (saveErr) {
        console.log(saveErr);
        return false;
      } else {
        return updatedTask;
      }
    });
  });
};

// ACTIVITY
// Controller function for getting a specific task
module.exports.getSpecificTask = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.log(error);
      return false;
    } else {
      return result;
    }
  });
};

// Controller function for completing a task
module.exports.completeTask = (taskId) => {
  return Task.findByIdAndUpdate(taskId, { status: "Complete" }).then(
    (result, error) => {
      if (error) {
        console.log(error);
        return false;
      } else {
        return Task.findById(taskId).then((updatedTask, saveErr) => {
          if (saveErr) {
            console.log(saveErr);
            return false;
          } else {
            return updatedTask;
          }
        });
      }
    }
  );
};
