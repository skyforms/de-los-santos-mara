// Syntax, Statements, and Comments

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

// Statements - instructions that we tell the computer to perform
// JS Statements usually end with a semi-colon

// Syntax - set of rules that describe how statements must be constructed

// Comments
// To create a comment, use Ctrl + /
// Single  (ctrl + / ) and multi-line (ctrl + shift + / ) comments

// console.log("Hello!");

/* alert("This is an alert!");
alert("This is another alert.");*/

// White space (spaces and line breaks) can impact functionality in many computer languages, but not in JS

// Variables
// Used to contain data

// Declare variables
// Tell our devices that a variableName is created and ready to store data
// Syntax
// let/const variableName;

let myVariable;
console.log(myVariable);

// Variables must be declared first before they are used
let hello;
console.log(hello);

// Declaring and initializing variables
// Syntax let/const variableName = initialValue;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// Reassigning variable values
productName = "Laptop";
console.log(productName);

// We cannot replace constant values unless in an array
// interest = 3.5;
// console.log(interest);

// Let variable cannot be re-declared within its scope
let friend = "Kate";
friend = "Jane";

// Declare a variable
let supplier;

// Initialization
supplier = "John Smith Tradings";

// Reassigning a value
supplier = "Zuitt Store;";

// Cannot declare a const variable without initialization in the same line
/* const pi;
pi = 3.1416;
console.log(pi); */

// Multiple variable declarations

let productCode = "DC017",
  productBrand = "Dell";

// let productCode = 'DC017';
// const productBrand = 'Dell';
console.log(productCode, productBrand);

// Data Types
// Strings - a series of characters that create a word, phrase, sentence, or anything related to creating text
let country = "Philippines";
let province = "Metro Manila";

// Concatenating strings
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// The escape character (\), in combination with other characters, can produce different effects

//"\n" refers to creating a new line in between text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = "John's employees went home early";
console.log(message);

// Numbers
let headcount = 26;
console.log(headcount);
let grade = 98.7;
console.log(grade);
let planetDistance = 2e10;
console.log(planetDistance);

// Combine text and strings
console.log("John's first grade last quarter is " + grade);

// Boolean
let isMarried = false;
let isGoodConduct = true;
console.log(isMarried);

console.log("isGoodConduct: " + isGoodConduct);

// Arrays
let grades = [98.7, 92.1, 90.7, 98.6];
console.log(grades);

// Objects

let myGrades = {
  firstGrading: 98.7,
  secondGrading: 92.1,
  thirdGrading: 90.7,
  fourthGrading: 94.6,
};

console.log(myGrades);

let person = {
  fullName: "Juan Dela Cruz",
  age: 35,
  isMarried: false,
  contact: ["+639123456789", "87000"],
  address: {
    houseNumber: "345",
    city: "Manila",
  },
};
console.log(person);

// typeof Operator
console.log(typeof person);
