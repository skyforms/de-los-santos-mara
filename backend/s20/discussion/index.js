console.log("Hello World");

// Arithmetic Operators
// +, 0, *, /, %

let x = 1397;
let y = 7831;

let sum = x + y;

console.log("Result of addition operator: " + sum);

// Difference
let difference = x - y;
console.log("Result of difference operation: " + difference);

// Product
let product = x * y;
console.log("Result of multiplication operation: " + product);

// Quotient
let quotient = x / y;
console.log("Result of division operation: " + quotient);

// Remainder
let remainder = x % y;
console.log("Result of modulo operation: " + remainder);

let a = 10;
let b = 5;

let remainderA = a % b;
console.log(remainderA); //0

// Assignment Operator
// Basic Assignment Operator (=)

let assignmentNumber = 8;

// Addition Assignment Operator
assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator
assignmentNumber -= 2;
console.log("Result of -= : " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of *= : " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of /= : " + assignmentNumber);

// Multiple Operators and Parentheses
/* 
    1. 3 * 4 = 12
    2. 12 / 5 = 2.4
    3. 1 + 2 = 3
    4. 3 - 2.4 = 0.6
*/
let mdas = 1 + 2 - (3 * 4) / 5;
console.log(mdas);

/*
    1. 4/5 = 0.8
    2. 2-3 = -1
    3. -1 * 0.8 = -0.8
    4. 1 + (-0.8) = 0.2
*/
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// Increment and Decrement
// Operators that add or subtract values by 1 and reassign the value of the variable where the increment/decrement was applied to

let z = 1;
// The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment"
// Increase, then reassign
let increment = ++z;

console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// The value of "z" is returned and stored in the variable "increment" then the value of z is increased by 1
// Reassign, then increase
increment = z++;

console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Decrease, then reassign
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Reassign, then decrease
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type Coercion
/*
    The automatic or implicit conversion of values from one data type to another
*/

let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(typeof coercion); // "1012"

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Boolean "true" is associated with 1
let numE = true + 1;
console.log(numE);

// Boolean "false" is associated with 0
let numF = false + 1;
console.log(numF);

// Comparison Operators

let juan = "juan";

// Equality Operator (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

// Inequality Operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);

// Strict Equality & Strict Inequality Operators

// Strict Equality Operator (===)
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === "1");
console.log("juan" === "juan");
console.log("juan" === juan);

// Strict Inequality Operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== "1");
console.log("juan" !== "juan");
console.log("juan" !== juan);

// Relational Operators

let abc = 50;
let def = 65;

let isGreaterThan = abc > def;
let isLessThan = abc < def;
let isGTOrEqual = abc >= def;
let isLTOrEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

let numStr = "30";
console.log(abc > numStr); // Forced coercion to turn the string into a number
console.log(def <= numStr);

let str = "twenty";
console.log(def >= str); // Javascript cannot coerce this string into a number

// Logical Operators

let isLegalAge = true;
let isRegistered = false;

// && (AND), || (OR), ! (NOT)

// &&
// Return true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered; // Returned as FALSE
console.log("Result of AND operator: " + allRequirementsMet);

// ||
// Returns true if one of the operands are true

let someRequirementsMet = isLegalAge || isRegistered; // Returned as TRUE
console.log("Result of OR operator: " + someRequirementsMet);

// ! 
// Returns the opposite value

let someRequirementsNotMet = !isRegistered;
console.log("Result of NOT operator: " + someRequirementsNotMet);
