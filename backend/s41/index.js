const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [Create To Do List App]
// [Section] Create To Do List Rouutes

// Allows app to read JSON data
app.use(express.json());
// Allows app to read data from forms
app.use(express.urlencoded({ extended: true }));

// Connecting to MongoDB Atlas
mongoose.connect(
  "mongodb+srv://maimdls:ZYtmiOtkjw159Vrq@cluster0.t6yo0c1.mongodb.net/taskDB?retryWrites=true&w=majority",
  {
    useNewURLParser: true,
    useUnifiedTopology: true,
  }
);

// Set notifications for connection success or failure
// Connection to the database
// Allows handling of errors when the initial connection is established
// Works with the on and once Mongoose methods
let db = mongoose.connection;

// If a connection error occurs - output in the console
// console.error.bind(console) - allows printing of errors in the console & terminal
// "Connection Error" - message to be displayed if an error is encountered
db.on("error", console.error.bind(console, "Connection Error"));

// If connection is successful - output in the console
db.once("open", () => console.log("You're connected to the cloud database."));

/* 
    Schemas
    - Schemas determine the structure of the documents to be written in the database
    - Act as blueprint for data
    - Use the Schema() constructor of the Mongoose module to create a new schema object
    - "new" - creates a new Schema
*/
const taskSchema = new mongoose.Schema({
  // Define the fields with the corresponding data type
  // For a task, it needs a "task name" and a "task status"
  name: String,
  // Field: "name", type: "String"
  status: {
    type: String,
    default: "Pending",
  },
});

// [Section] Models
// Uses schemas; used to create/instantiate objects that correspond to the schema
// Models use schemas, and act as a "middleman" from the server (JS code) to our database
/* 
    Data Flow:
    Server > Schema (Blueprint) > Database > Collection
*/
// The variable/object "Task" can be used to run commands for interacting with the database
const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (req, res) => {
  // "findOne" - a Mongoose method that acts similarly to "find" in MongoDB
  // findOne() returns the first document that matches the search criteria as a single object
  Task.findOne({ name: req.body.name }).then((result, err) => {
    // If a document was not found and the document's name matches the information sent via client/Postman - returns "Duplicate task found"
    if (result != null && result.name == req.body.name) {
      return res.send("Duplicate task found");

      // If no document was found
    } else {
      // Create a new task and save it to the database
      let newTask = new Task({
        name: req.body.name,
      });

      // The save() method stores the information in the database
      // Since "newTask" was created/instantiated from the Mongoose Schema, it will gain access to this method to save to the database
      // save() can send the result or error in another JS method called then()
      newTask.save().then((savedTask, saveErr) => {
        if (saveErr) {
          // If error occurs, it will log the error
          return console.error(saveErr);
        } else {
          // Returns status code "201" and msg upon successful creation
          return res.status(201).send("New task created");
        }
      });
    }
  });
});

// Get all tasks
app.get("/tasks", (req, res) => {
  // "find" - Mongoose method that is similar to MongoDB's "find"
  // empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback fnction
  Task.find({}).then((result, err) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

// Activity

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  User.findOne({ username: req.body.username }).then((result, err) => {
    if (err) {
      return console.log(err);
    } else {
      if (result != null && result.username == req.body.username) {
        return res.send("Duplicate username found");
      } else {
        if (req.body.username && req.body.password) {
          let newUser = new User({
            username: req.body.username,
            password: req.body.password,
          });
          newUser.save().then((savedUser, saveErr) => {
            if (saveErr) {
              return console.error(saveErr);
            } else {
              return res.status(201).send("New user registered");
            }
          });
        } else {
          return res
            .status(400)
            .send("Both username and password must be provided");
        }
      }
    }
  });
});

// Listen to port - if the port is accessed, server will run
if (require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
