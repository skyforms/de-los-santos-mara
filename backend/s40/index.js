/* 
    npm init
    - Used to initialize a new package.json file
    package.json
    - contains info about the project, dependencies, and other settings
    package-lock.json - locks the versions of all installed dependencies
*/

const express = require("express");
const app = express();
const port = 4000;

// Middleware Functions
// A software that provides common services & capabilities to an application outside of what's offered by the operating system

// app.use()
// A method used to run another function or method for an Express JS api; used to run middleware

// Allows our app to parse incoming JSON data
app.use(express.json());
// Allows our app to read data from forms, and to receive information in other data types such as an object (which will be used throughout the application)
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/hello", (req, res) => {
  res.send("Hello from /hello endpoint!");
});

/* app.post("/hello", (req, res) => {
  res.send("Hello from the post route!");
}); */

/* 
    Mini-Activity 1
        - Refactor the post route to receive data from the req.body
        - Send a message that has the data received 
    
    Message: "Hello, FirstName LastName!"
*/

app.post("/hello", (req, res) => {
  let firstName = req.body.firstName;
  let lastName = req.body.lastName;
  res.send(`Hello, ${firstName} ${lastName}!`);
});

// Mini-Activity 2
// Add PUT and DELETE

app.put("/", (req, res) => {
  res.send("Hello from a put method route!");
});

app.delete("/", (req, res) => {
  res.send("Hello from a delete method route!");
});

// Mock DB to store objects when the signup route is accessed
let users = [];

// [Route for Adding a New User]
/*
app.post("/signup", (req, res) => {
  console.log(req.body);
  users.push(req.body);
  res.send(users);
});
*/

// Mini-Activity 3
// Refactor the sign up route

/* 
app.post("/signup", (req, res) => {
  if (req.body.username && req.body.password) {
    users.push(req.body);
    res.send(`Thanks for signing up, ${req.body.username}!`);
  } else if (!req.body.username) {
    res.send(`Error! Username is required.`);
  } else if (!req.body.password) {
    res.send(`Error! Password is required.`);
  }
});
*/

app.post("/signup", (req, res) => {
  console.log(req.body);
  if (req.body.username !== "" && req.body.password !== "") {
    users.push(req.body);
    res.send(`User ${req.body.username} has been successfully registered.`);
  } else {
    res.send(`Please input both username and password.`);
  }
});

// [Change Password Route]
app.put("/change-password", (req, res) => {
  let message;

  for (let i = 0; i < users.length; i++) {
    if (req.body.username == users[i].username) {
      users[i].password = req.body.password;
      message = `User ${req.body.username}'s password has been updated.`;
      console.log(users);
      break;
    } else {
      message = `User does not exist!`;
    }
  }
  res.send(message);
});

// Activity
app.get("/home", (req, res) => {
  res.send("Welcome to the home page!");
});

app.get("/users", (req, res) => {
  res.send(users);
});

app.delete("/delete-user", (req, res) => {
  let message;
  let userFound = false;

  for (let i = 0; i < users.length; i++) {
    if (req.body.username == users[i].username) {
      users.splice(i, 1);
      userFound = true;
      message = `User ${req.body.username} has been deleted.`;
      break;
    }
  }

  if (userFound) {
    res.send(message);
  } else {
    if (users.length === 0) {
      message = "No users found.";
    } else {
      message = "User does not exist!";
    }
    res.send(message);
  }
});
// End of Activity

// For checking activity
// Do not remove
if (require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
