const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/hello", (req, res) => {
  res.send("Hello from /hello endpoint");
});

app.post("/hello", (req, res) => {
  res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

app.put("/", (req, res) => {
  res.send("Hello from a put method route!");
});

app.delete("/", (req, res) => {
  res.send("Hello from a delete method route!");
});

let users = [];

app.post("/signup", (req, res) => {
  console.log(req.body);

  if (req.body.username !== "" && req.body.password !== "") {
    users.push(req.body);
    res.send(`User ${req.body.username} has been successfully registered`);
  } else {
    res.send(`Please input BOTH username and password`);
  }
});

app.put("/change-password", (req, res) => {
  let message;

  for (let i = 0; i < users.length; i++) {
    if (req.body.username == users[i].username) {
      users[i].password = req.body.password;

      message = `User ${req.body.username}'s password has been updated!`;

      console.log(users);

      break;
    } else {
      message = "User does not exist";
    }
  }

  res.send(message);
});

// Activity
app.get("/home", (req, res) => {
  res.send("Welcome to the home page!");
});

app.get("/users", (req, res) => {
  res.send(users);
});

app.delete("/delete-user", (req, res) => {
  let message;
  let userFound = false;

  for (let i = 0; i < users.length; i++) {
    if (req.body.username == users[i].username) {
      users.splice(i, 1);
      userFound = true;
      message = `User ${req.body.username} has been deleted.`;
      break;
    }
  }

  if (userFound) {
    res.send(message);
  } else {
    if (users.length === 0) {
      message = "No users found.";
    } else {
      message = "User does not exist!";
    }
    res.send(message);
  }
});

//End of Activity

if (require.main === module) {
  app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
