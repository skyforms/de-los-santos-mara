let http = require("http");

// Require directive - used to include and load Node.js modules/packages
// Module - software component or part of a program that contains one or more rotines
// Objects that contain pre-built codes, methods, and data

/* 
  http (HyperText Transfer Protocol)
  - default module that comes from NodeJS
  - Allows us to transfer data and use methods to let us create servers
  - A protocol that allows the fetchinng of resources such as HTML documents
  - Protocol for client server communication
        Server/application: http://localhost:4000
*/

/* 
  Client
  - An application which creates requests for resources from a server
  - Triggers an action (in the web development context) through a URL, and waits for the response of the server
*/

/* 
  Server
  - Can host/deliver resources that are requested by a client
  - Can handle multiple clients at once
*/

/* 
  Node.js
  - A runtime environment that allows us to create/develop a backend or server-side application using JavaScript
  - JS, by default, was conceptualized for front-end use

  Runtime Environment
  - An environment in which a program or application is executed
*/
http
  .createServer(function (request, response) {
    /* 
        Role of .createServer() 
        - a method from the HTTP module that allows us to create a server, and handle requests/responses from clients/server respectively
        - takes a function argument, which is able to receive 2 objects:
          1. Request object - contains details from the client
          2. Response object - contains details from the server
        - ALWAYS receives the request object first before the response


      */

    response.writeHead(200, { "Content-type": "text/plain" });
    /* 
      response.writeHead()
      - method of the response object that allows us to add headers to our responses

      1. HTTP Status Code
      - 200 - OK
      - 404 - Not Found
      2. "Content-type"
      - Pertains to the data type of the response
    */

    response.end("Hi, my name is Hachi!");
    /* 
      response.end()
      - Ends the response from the server
      - Can send a message/data as a string
    */
  })
  .listen(4000);

/* 
  .listen()
  - Allows us to assign a port to our server
  - Allows us to serve our index.js server in our local machine, assigned to port 4000

  4000, 4040, 8000, 5000, 3000, 4200 - used for web development
*/

console.log("Server is running at localhost:4000");
