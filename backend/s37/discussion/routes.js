// Servers can respond differently to different requests
/* 
    - Requests are issued by the client through the use of URLs
    - A client can issue a different response by using a different URL

    Ex.
    http://localhost:4000/
    http://localhost:4000/profile

    / = URL endpoint (default)
    /profile = different URL endpoint
*/

const http = require("http");

// Creates a variable to store the port number
const port = 4000;

const app = http.createServer((request, response) => {
  // Information about the URL endpoint of the request is in the request object
  // Different requests require different responses
  // Route - process/way to respond differently to a request
  if (request.url == "/greeting") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Hello, B297!");
  } else if (request.url == "/homepage") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("This is the homepage.");
  } else {
    response.writeHead(404, { "Content-type": "text/plain" });
    response.end("Page not available, try other routes.");
  }

  // Mini-Activity
  // Create an else conddition that all other routes will return a message of "Page Not Available"
});

app.listen(port);
console.log(`Server is now accessible at localhost: ${port}`);
