console.log("Javascript Objects!");
// [Objects]
/* 
    A data type used to represent real world objects
    Information stored in objects are represented by:
        key: value
    Keys are also referred to as a "property" of an object
    Different data types can be stored in an object's properties, creating complex data structures
*/

// Creating objects using object initializer/literal notation
/*
    Syntax
    let objectName = {
        keyA: valueA,
        keyB: valueB
    }
*/

let ninja = {
  name: "Naruto",
  village: "Konoha",
  occupation: "Hokage",
};

console.log("Result from creating objects using initalizers");
console.log(ninja);
console.log(typeof ninja);

let dog = {
  name: "Tiger",
  color: "black",
  breed: "Labrador Retriever",
};

// Creating objects using a constructor function
// Constructor functions are named with a capital letter as convention

/*
    Constructor Functions allow us to create several objects that have the same data structure
    Instance - concrete occurance of any object  which emphasizes its distinct/unique identity
*/

/* 
    function ObjectName(key1, key2){
        this.key1 = key1;
        this.key2 = key2;
    }
*/

function Laptop(name, manufactureDate) {
  // "This" is used to assign object properties by associating with the values received in the parameters
  // Allows us to assign a new object's properties by associating them
  // with values received from a constructor function's parameter
  this.name = name;
  this.manufactureDate = manufactureDate;
}

// Instances
/*
    "New" operators create an instance of an object
    Object instances are often interchanged because object literals (let object = {})
    Instances (let object = new Object) are distinct/unique objects
*/

let laptop1 = new Laptop("Lenovo", 2022);
console.log("Result from creating objects using object constructor");
console.log(laptop1);

let myLaptop = new Laptop("MacBook Air", 2020);
console.log("Result from creating objects using object constructor");
console.log(myLaptop);

/* 
    Invoke/call "Laptop" function instead of creating a new object instance 
    Returns undefined without the "new" operator because the "Laptop" function
    does not have a return statement
*/

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating instance without new keyword");
console.log(oldLaptop);

// Mini-Activity

let laptop2 = new Laptop("MSI Katana 15", 2023);
console.log("Result from creating objects using object constructor");
console.log(laptop2);

let laptop3 = new Laptop("Razer Blade 16", 2023);
console.log("Result from creating objects using object constructor");
console.log(laptop3);

let laptop4 = new Laptop("ROG Zephyrus G14", 2023);
console.log("Result from creating objects using object constructor");
console.log(laptop4);

// Create empty objects

let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

// [Access Object Properties]

// dot notation
console.log("Result of dot notation: " + myLaptop.name);
console.log("Result of dot notation: " + myLaptop.manufactureDate);
// square bracket notation
console.log("Result of square bracket notation: " + myLaptop["name"]);
console.log(
  "Result of square bracket notation: " + myLaptop["manufactureDate"]
);

// Access Array objects
/*
    Accessing array elements can also be done using the square brackets
    Accessing object properties using square bracket notation 
    and array indexes can cause confusion
*/

let array = [laptop1, myLaptop];

// Square bracket
// May be confused for accessing array indexes
console.log(array[0]["name"]);

// Dot notation
// Differentation between accessing arrays and object properties
// This tells us that array[0] is an object by using the dot notation
console.log(array[0].name);

// [Initialize, Add, Delete, Reassign Object Properties]
// Useful for when object properties are undefined at the time of creation

let car = {};
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation:");
console.log(car);

// car.number = [1,2,3];
// console.log(car);

car["manufacture date"] = 2019;
console.log(car["manufacture date"]);
// console.log(car.manufacture date); - results in a syntax error
console.log(car);

// Delete object properties
delete car["manufacture date"];
console.log("Result from deleting properties");
console.log(car);

// Reassign object properties
car.name = "Dodge Charger";
console.log("Result from reassigning properties");
console.log(car);

// [Object Methods]
/* 
    A method is a function which is a property of an object
    Similar to functions/features of real world objects, methods are defined
    based on what an object is capable of doing, and how it should work
*/

let person = {
  name: "Cardo",
  talk: function () {
    console.log("Hello my name is " + this.name);
  },
};

console.log(person);
console.log("Result of object methods: ");
person.talk();

// Add another method to an object
// Walk property
person.walk = function () {
  console.log(this.name + " walked a thousand miles forward!");
};
person.walk();

let friend = {
  firstName: "Nami",
  lastName: "Luffy",
  address: {
    city: "Tokyo",
    country: "Japan",
  },
  emails: ["nami@sea.com", "namiluffy@gmail.com"],
  introduce: function () {
    console.log("Hello! My name is " + this.firstName + " " + this.lastName);
  },
};

friend.introduce();

// [Real World Application of Objects]

/* 
    Scenario: 
    - Create a game that has several Pokemon interact with each other
    - Every Pokemon has the same set of stats, properties, and functions
*/

// Use object literals

let myPokemon = {
  name: "Pikachu",
  level: 3,
  health: 100,
  attack: 50,
  tackle: function () {
    console.log("This Pokemon tackled Target Pokemon!");
    console.log(
      "Target Pokemon's health is now reduced to Target Pokemon Health"
    );
  },
  faint: function () {
    console.log("Pokemon fainted!");
  },
};

console.log(myPokemon);
myPokemon.faint();

// Create an object constructor

function Pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  // Methods
  this.tackle = function (target) {
    console.log(this.name + " tackled " + target.name + "!");
    console.log(
      target.name + "'s health is now reduced to _targetPokemonHealth_"
    );
  };

  this.faint = function () {
    console.log(this.name + " fainted!");
  };
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);
pikachu.tackle(rattata);
rattata.faint();

// Member 5
/*
For the following exercise, label the steps within the code and do NOT use var to name variables. Only use let or const.

Step 1.
Create a trainer object using object literals.
Initialize the following trainer object properties: 
Name (String)
Age (Number)
Pokemon (Array)
Friends (Object with Array values for properties)

Step 2.
Initialize the trainer object method named talk that returns the message "Pikachu! I choose you!"
Access the trainer object properties using dot and square bracket notation.
Invoke the trainer talk object method in a console.log() to display the value returned by the method.

Step 3. 
Create a constructor function called Pokemon for creating a pokemon with the following properties:
Name (Provided as argument to the constructor)
Level (Provided as an argument to the constructor)
Health (Create an equation that uses the level property)
Attack (Create an equation that uses the level property)

Step 4.
Add and create a tackle method for the pokemon constructor that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
Return a message that the pokemon has been tackled.
Add and create a faint method for the pokemon constructor that will return a message of targetPokemon has fainted.

Step 5.
Add a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
Invoke the tackle method of one pokemon object to see if it works as intended.
Create several pokemon object from the constructor with varying and level properties.
Use if and else statements for this step.
*/

let mareanie = new Pokemon("Mareanie", 38);
let entei = new Pokemon("Entei", 99);
let mimikyu = new Pokemon("Mimikyu", 23);
let oshawott = new Pokemon("Oshawott", 5);
let tepig = new Pokemon("Tepig", 5);

