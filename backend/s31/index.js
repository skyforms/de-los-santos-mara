/*
    Lecture: Introduction to JSON
        - Learn about a popular format of data used to transform/receive it
    
    Objectives
    - Learn the definition and usage of JSON
    - Learn how to create JSON
    - Learn how to create JSON out of JS Objects
    - Learn how to parse JS Objects out of JSON

    JSON - JavaScript Object Notation
    - Data used by applications to store and transport data to one another
    - Use is not limited to JS-based applications; also used in other programming languages (Java, PHP, Python)
    - File extension: .json

    Why do we need JSON?
    - Example of JSON: 
    { 
        "to": "Tove",
        "from": "Jani",
        "heading": "Reminder",
        "body": "Don't forget me this weekend!"
    }
    - Meaning can be deciphered at a glance - easy to understand
*/

console.log("Intro to JSON!");

/*
    JSON

    Syntax:
    {
        "propertyA": "valueA",
        "propertyB": "valueB"
    };

    - Wrapped in curly brackets
    - Properties/keys are wrapped in double quotations

*/

let sample1 = `

    {
        "name": "Jason Derulo",
        "age": 20,
        "address":{
            "city": "Quezon City",
            "country": "Philippines"
        }
    }
`;

// Data type of sample1? STRING (wrapped in string literals)
console.log(sample1);
console.log(typeof sample1);

// Can you turn JSON into JS Objects? YES
// Method
console.log(JSON.parse(sample1));

// JSON Array
let sampleArr = `
    [
        {
            "email": "wackywizard@example.com",
            "password": "laughOutLoud123",
            "isAdmin": true
        },
        {
            "email": "dalisay.cardo@gmail.com",
            "password": "alyanna4ever",
            "isAdmin": true
        },
        {
            "email": "lebroan4goat@ex.com",
            "password": "ginisamix",
            "isAdmin": false
        },
        {
            "email": "ilovejson@gmail.com",
            "password": "jsondontloveme",
            "isAdmin": false
        }
    ]
`;

// Can we use array methods on a JSON array? NO - it is considered a string
console.log(typeof sampleArr);

// Solution: Convert the JSON array to a JavaScript using parse and save it in a variable
let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);
console.log(typeof parsedSampleArr);

// Mini-Activity 1
// Instructions: Delete the last item in the JSON Array
console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

// To send this data back to the client/frontend, it must be in JSON format
// JSON.parse does NOT mutate or update the original JSON
// You can turn a JS object into JSON again using the ff:
//  JSON.stringify()

// Reassigning sampleArr
sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);
console.log(typeof sampleArr);

// Database (JSON) -> Server/API (Json to JS Object to process ) -> Send as JSON -> Frontend/Client

let jsonArr = `
    [
        "pizza",
        "hamburger",
        "spaghetti",
        "shanghai",
        "hotdog stick on pineapple", 
        "pancit bihon"
    ]
`;

// Mini-Activity 2
/*
    Instructions: 
    - Convert the array to a JS object for manipulation
    - Delete the last item in the array
    - Add a new item in the array
    - Stringify the array back into JSON
    - Update the JSON array with the stringified array
*/

let parsedFoodArr = JSON.parse(jsonArr);
console.log(parsedFoodArr);

// Delete last item
console.log(parsedFoodArr.pop());
// Add new item
console.log(parsedFoodArr.push("fried rice"));
// Convert to JSON
jsonArr = JSON.stringify(parsedFoodArr);
console.log(jsonArr);

// Gather User Details
let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
  city: prompt("What city do you live in?"),
  country: prompt("What country does your city address belong to?"),
};

let otherData = JSON.stringify({
  firstName: firstName,
  lastName: lastName,
  age: age,
  address: address,
});

console.log(otherData);
