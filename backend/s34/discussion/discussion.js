// CRUD Operations
// Heart of any backend application

// Create
// Insert one document
/*
        Syntax:
        db.collectionName.insertOne({object})
    */

db.users.insertOne({
  firstName: "John",
  lastName: "Smith",
});

db.users.insertOne({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
    phone: "87000",
    email: "janedoe@gmail.com",
  },
  courses: ["CSS", "JavaScript", "Python"],
  department: "none",
});

// Insert Many
/* 
    Syntax:
    db.collectionName.insertMany([{},{},{}])
*/

db.users.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87001",
      email: "stephenhawking@gmail.com",
    },
    courses: ["Python", "React", "PHP"],
    department: "none",
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "87002",
      email: "neilarmstrong@gmail.com",
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none",
  },
]);

// [Read] Finding Documents
// Finding a single document
/*
    Syntax:
    db.collectionName.find()
    Leaving the search criteria empty retrieves all documents within the collection

    db.collecitonName.find({ field: value })
*/
db.users.find();

db.users.find({ firstName: "Stephen" });

// Finding document with multiple parameters
/* 
    Syntax: 
    db.collectionName.find({fieldA: valueA, fieldB:valueB})
*/

db.users.find({ lastName: "Armstrong", age: 82 });

// Update

// Test document to use update on
db.users.insertOne({
  firstName: "Test",
  lastName: "Test",
  age: 0,
  contact: {
    phone: "00000",
    email: "test@gmail.com",
  },
  courses: [],
  department: "none",
});

// Update One
/* 
    Syntax:
    db.collectionName.updateOne({criteria},{$set: {}})
*/
// $set - Replaces the value of a field with the specified value
db.users.updateOne(
  { firstName: "Test" },
  {
    $set: {
      firstName: "Bill",
      lastName: "Gates",
      age: 65,
      contact: {
        phone: "12345678",
        email: "bill@gmail.com",
      },
      courses: ["PHP", "Laravel", "HTML"],
      department: "Operations",
      status: "Active",
    },
  }
);

// Updating Multiple Documents
/* 
  Syntax:
  db.collectionName.updateMany({criteria}, {$set:{field, value}})
*/

db.users.updateMany(
  {
    department: "none",
  },
  {
    $set: {
      department: "HR",
    },
  }
);

// Delete

// Add test entry to delete
db.users.insert({
  firstName: "test",
});

// Deleting a single document
/* 
    Syntax:
    db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({
  firstName: "test",
});

// Delete Many
// Without a search criteria, ALL DOCUMENTS WITHIN A COLLECTION WILL BE DELETED!
// Do NOT use: databaseName.collectionName.deleteMany()

db.users.deleteMany({
  firstName: "Bill",
});

// 2. Insert multiple rooms (insertMany method)  in the rooms collection

// 2. Insert multiple rooms (insertMany method)  in the rooms collection

async function addManyFunc(db) {

    await (

        db.hotel.insertMany([{
    name: "double",
    accomodates: 2,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false,
  }, {
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway"
    rooms_available: 15,
    isAvailable: false,
  }]);

    );

   return(db);

};

async function addManyFunc(db) {
  await ;

  return db;
}
