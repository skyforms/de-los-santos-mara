// JavaScript - DOM Manipulation & Reactive DOM with Fetch
// Manipulate and add interactive events on an HTML document using JavaScript

/*
    Events:
        Change
        Click
        Load
        Keydown
        Keyup 

*/

console.log("Hi from index.js!");

// fetch() method has one argument by default: the url
/* 
    The url is a representative address of accessing a resource/data in another machine
    For now - jsonplaceholder url will act as a sample server where data can be fetched from
    The .then method allows for the processing of data retrieved using fetch
    The response is simply a parameter name. As a convention, response is used as an indication that the response from the server will be processed
    It is a representation of what was "received" from the server
    response.json() is a method to convert the incoming data as a proper JS object which can be further processed
*/

// Another .then() method can be added to do something with the processed server response

// Get, read, or retrieve
fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((data) => {
    //console.log(data);
    showPosts(data);
  });

// const showPosts = function (posts){}

//Receive fetched data as argument
const showPosts = (posts) => {
  console.log(posts);
  // Add each post as a string
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
        <div id="post-${post.id}">
            <h3 id="post-title-${post.id}">${post.title}</h3>
            <p id="post-body-${post.id}">${post.body}</p>
            <button onclick="editPost("${post.id}")">Edit</button>
            <button onclick="deletePost('${post.id}')">Delete</button>
        </div>
    `;
  });

  // console.log(postEntries);

  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
  // By default, the page is refreshed around form submission. To prevent this, pass the event parameter to the function and use the preventDefault() method
  event.preventDefault();

  let titleInput = document.querySelector("#txt-title");
  let bodyInput = document.querySelector("#txt-body");
  /*
  console.log(titleInput.value);
  console.log(bodyInput.value);

  console.log("Hello! The form has been submitted!");
  */

  // Whenever we try to add, update, and delete data from a server, another argument must be passed to the fetch() method that contains other details
  // fetch("<URL>", {options})
  // Options object:
  /* 
    Methods - tells the server what we intend to do
    Values passed here are called HTTP methods:
        GET - For retrieving data from a server
        POST - For adding data to a server
        PUT - For updating data in a server 
        DELETE 
    Body: This property contains the main content to be sent to the servers
  */

  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: titleInput.value,
      body: bodyInput.value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully added!");

      // Clear the input elements after submission
      titleInput.value = null;
      bodyInput.value = null;
    });

  alert("Successfully added!");
});

// Edit Post

const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// Update Post

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();

  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    body: JSON.stringify({
      id: document.querySelector("#txt-edit-id").value,
      title: document.querySelector("#txt-edit-title").value,
      body: document.querySelector("#txt-edit-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully updated!");

      // Clear the input elements after submission
      document.querySelector("#txt-edit-id").value = null;
      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-body").value = null;
      document
        .querySelector("#btn-submit-update")
        .setAttribute("disabled", true);
    });
});

// Activity
// Delete Button

const deletePost = (id) => {
  fetch("https://jsonplaceholder.typicode.com/posts/1", { method: "DELETE" });
  document.querySelector(`#post-${id}`).remove();
};

document.querySelector(`#delete-all`).addEventListener("click", () => {
  document.querySelector(`#div-post-entries`).innerHTML = "";
  alert("All posts deleted!");
});
