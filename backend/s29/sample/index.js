console.log("DOM Manipulation");

console.log(document);
console.log(document.querySelector("#clicker"));

// document refers to the entire webpage
/* 
    querySelector 
    - used to select a specific element obj within the html element
    - takes a string input that is formatted like CSS selector
    - can select elements regardless of whether or not the string is an id, class, or tag, as long as the element is existing in the webpage
*/

/* 
    Alternative methods that we use aside from querySelector in retrieving elements:
        document.getElementByID();
        document.getElementByClassName();
        document.getElementByTagName()
*/

let counter = 0;

const clicker = document.querySelector("#clicker");
clicker.addEventListener("click", () => {
  console.log("The button has been clicked");
  counter++;
  // alert(`The button has been clicked ${counter} times`);
  console.log(`The button has been clicked ${counter} times`);
});

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// Mini-Activity

/* txtFirstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
});

txtLastName.addEventListener("keyup", () => {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
});*/

/* function updateFullName() {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);

const labelFirstName = document.querySelector("#label-txt-first-name");*/

const updateFullName = (e) => {
  let firstName = txtFirstName.value;
  let lastName = txtLastName.value;

  spanFullName.innerHTML = `${firstName} ${lastName}`;
};

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);

labelFirstName.addEventListener("mouseover", (e) => {
  alert("You hovered the first name label!");
});
