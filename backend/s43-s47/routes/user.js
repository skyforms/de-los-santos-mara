/* 
    NAMING CONVENTIONS
    Models - Capitalized (i.e., models folder > User.js)
    Routes - small caps (i.e., routes folder > user.js)
*/

// Dependencies
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// [Routes]

// Check Email
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Register a User
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// User Authentication
router.post("/login", userController.loginUser);

// User Details
router.post("/details", verify, userController.getProfile);

// Route to enroll a verified user to a course
router.post("/enroll", verify, userController.enroll);

// ACTIVITY: Retrieve enrolled courses of the logged-in user
router.get("/getEnrollments", verify, userController.getEnrollments);

// POST route for resetting the password
router.post("/reset-password", verify, userController.resetPassword);

// Update User Profile
router.put("/profile", verify, userController.updateProfile);

// ACTIVITY S48: Update User as Admin
router.put(
  "/update-admin/:userId",
  verify,
  verifyAdmin,
  userController.updateUserAsAdmin
);

// Update Enrollment Status of a User for a Course
router.put(
  "/enrollmentStatusUpdate",
  verify,
  verifyAdmin,
  courseController.updateEnrollmentStatus
);

// [Export Route System]
module.exports = router;
