// Controller

const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email exists already
/* 
    Steps:
    1. "Find" - Mongoose method to find duplicate items
    2. "Then" - Method to send a response back to the FE Application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqbody) => {
  return User.find({ email: reqbody.email }).then((result) => {
    // Find method returns a record if a match is found
    if (result.length > 0) {
      return true;
    } else {
      // No duplicate found
      // User is not yet registered in DB
      return false;
    }
  });
};

// User Registration
/* 
    Business Logic
    - Create a new User object using the Mongoose model & information from req.body
    - Make sure that the password is encrypted
    - Save the new User to the database 
*/

module.exports.registerUser = (reqbody) => {
  let newUser = new User({
    firstName: reqbody.firstName,
    lastName: reqbody.lastName,
    email: reqbody.email,
    mobileNo: reqbody.mobileNo,
    password: bcrypt.hashSync(reqbody.password, 10),
  });
  return newUser
    .save()
    .then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    })
    .catch((err) => err);
};

// User Authentication
/* 
    Business Logic
    - Check the DB for if the user email exists
    - Compare the password from req.body and password stored in DB
    - Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        return false;
      } else {
        // compareSync is used to compare a non-encrypted password and the encrypted password from the DB
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          result.password
        );
        // If PW matches
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(result) });
        }
        // Else PW does not match
        else {
          return res.send(false);
        }
      }
    })
    .catch((err) => res.send(err));
};

// Retrieve User Details

module.exports.getProfile = (req, res) => {
  return User.findById(req.user.id)
    .then((result) => {
      result.password = "";
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// Enroll Authenticated User
/* 
  Business Logic
  1. Find the document in the database using the user's ID
  2. Add the course ID to the users enrollment array
  3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (req, res) => {
  // Confirm user ID and Course ID in terminal
  console.log(req.user.id);
  console.log(req.body.courseId);

  // Check if user is admin
  if (req.user.isAdmin) {
    return res.send("Action Forbidden");
  }

  // Creates an "isUserUpdated" variable and returns true upon successful update; otherwise, returns error
  let isUserUpdated = await User.findById(req.user.id).then((user) => {
    let newEnrollment = {
      courseId: req.body.courseId,
    };

    // Add courseId in an object, and push object into the user's "enrollment" array
    user.enrollments.push(newEnrollment);

    // Save updated user and return true if successful, or error if failed
    return user
      .save()
      .then((user) => true)
      .catch((err) => err.message);
  });

  // Checks if there are errors in updating the user
  if (isUserUpdated !== true) {
    return res.send({ message: isUserUpdated });
  }

  let isCourseUpdated = await Course.findById(req.body.courseId).then(
    (course) => {
      let enrollee = {
        userId: req.user.id,
      };
      course.enrollees.push(enrollee);
      return course
        .save()
        .then((course) => true)
        .catch((err) => err.message);
    }
  );

  // Checks if there are errors in updating the couurse
  if (isCourseUpdated !== true) {
    return res.send({ message: isCourseUpdated });
  }

  // Checks if both user and course update were successful
  if (isUserUpdated && isCourseUpdated) {
    return res.send({ message: "Enrolled successfully." });
  }
};

// ACTIVITY: Get Enrollments
module.exports.getEnrollments = (req, res) => {
  return User.findById(req.user.id)
    .then((result) => {
      return res.send(result.enrollments);
    })
    .catch((err) => res.send(err));
};

// Controller to reset user password
module.exports.resetPassword = async (req, res) => {
  // Used object destructuring
  const { id } = req.user;
  const { newPassword } = req.body;

  try {
    // Hash the new password before updating
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Update the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    return res.send({ message: "Password reset successful" });
  } catch (error) {
    return res.status(500).send({ message: "Error resetting password" });
  }
};

// Controller function to update the user profile
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to update profile" });
  }
};

// ACTIVITY S48: Update User as Admin
module.exports.updateUserAsAdmin = async (req, res) => {
  const userIdToUpdate = req.params.userId;

  try {
    const updatedUser = await User.findByIdAndUpdate(
      userIdToUpdate,
      { isAdmin: true }, // Set isAdmin to true to update user as admin
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json({ message: "User updated as admin successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to update user as admin" });
  }
};

// ACTIVITY S48 STRETCH GOAL
module.exports.updateEnrollmentStatus = async (req, res) => {
  try {
    const { userId, courseId, status } = req.body;

    // Find the user and update the enrollment status
    const user = await User.findById(userId);

    // Find the enrollment for the course in the user's enrollments array
    const enrollment = user.enrollments.find(
      (enrollment) => enrollment.courseId === courseId
    );

    if (!enrollment) {
      return res.status(404).json({ error: "Enrollment not found" });
    }

    enrollment.status = status;

    // Save the updated user document
    await user.save();

    res.status(200).json({ message: "Enrollment status updated successfully" });
  } catch (error) {
    res.status(500).json({
      error: "An error occurred while updating the enrollment status",
    });
  }
};
