const Course = require("../models/Course");
const User = require("../models/User");

// ACTIVITY: Add Course Controller
module.exports.addCourse = (req, res) => {
  const newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newCourse
    .save()
    .then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        console.error(newCourse);
        return res.send(true);
      }
    })
    .catch((err) => res.send(err));
};

module.exports.getAllCourses = (req, res) => {
  return Course.find({}).then((result) => {
    return res.send(result);
  });
};

module.exports.getAllActiveCourses = (req, res) => {
  return Course.find({ isActive: true }).then((result) => {
    return res.send(result);
  });
};

module.exports.getCourse = (req, res) => {
  return Course.findById(req.params.courseId).then((result) => {
    return res.send(result);
  });
};

module.exports.updateCourse = (req, res) => {
  let updatedCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };

  return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then(
    (course, error) => {
      if (error) {
        return res.send(false);
      } else {
        res.send(true);
      }
    }
  );
};

module.exports.archiveCourse = (req, res) => {
  return Course.findByIdAndUpdate(req.params.courseId, { isActive: false })
    .then((course) => {
      if (course) {
        return res.send(true);
      } else {
        return res.send(false);
      }
    })
    .catch((error) => {
      return res.send(false);
    });
};

module.exports.activateCourse = (req, res) => {
  const courseId = req.params.courseId;

  return Course.findByIdAndUpdate(courseId, { isActive: true }).then(
    (course, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    }
  );
};

// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: "i" },
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

// Get Emails of Enrolled Users
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Use req.params to get route parameters

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map((enrollee) => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } });

    // Extract the emails from the enrolled users
    const userEmails = enrolledUsers.map((user) => user.email);

    res.status(200).json({ userEmails });
  } catch (error) {
    res
      .status(500)
      .json({ message: "An error occurred while retrieving enrolled users" });
  }
};

// ACTIVITY S48: Search Courses by Price Range
module.exports.searchCoursesByPrice = async (req, res) => {
  const { minPrice, maxPrice } = req.body;

  try {
    const courses = await Course.find({
      price: { $gte: minPrice, $lte: maxPrice },
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};
