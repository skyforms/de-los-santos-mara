console.log("JavaScript Loops");

/* 
    Mini-Activity 
    1. Create a function named greeting displaying
        a message you want to say to yourself using console.log
    2. Invoke the greeting() function 5 times
    3. Take a screenshot
*/

function greeting() {
  console.log("ここまで来たなんて素晴らしい！");
}

let countNum = 50;

while (countNum !== 10) {
  console.log("This is printed inside the sample loop: " + countNum);
  greeting();
  countNum--;
}

// [While Loop]
// Takes in an expression/condition
// Expressions are any unit of code that can be evaluated to a value
// A loop will iterate to a certain number of times until an expression or condition is met
// Iteration - term given to the repetition of statements

/*
    Syntax:
        while(expression/condition){

        }
*/

let count = 5;

// While the value of count is not equal to 0
while (count !== 0) {
  //The current value of count is printed out
  console.log("While: " + count);
  // Decreases the value of count by 1 every iteration to stop the loop when it reaches 0
  count--;
}

// [Do While Loop]
/* 
    Works like the while loop - but unlike WHILE LOOPS, 
    these guarantee that the code will be executed at least once

    Syntax:
        do {
            statement
        } while(expression/condition);
*/

let number = Number(prompt("Give me a number: "));
// Search: difference between parseInt and Number

do {
  console.log("Do While: " + number);
  number += 1;
  // += addition assignment operator
} while (number < 10);

// [For Loop]
/* 
    Consists of 3 parts:
    1. Initialization: value that will track the progression of the loop
    2. Expression/Condition: will be evaluated to determine whether the loop will run one more time 
    3. Final expression: indicates how to advance the loop

    Syntax:
        for (initialization; expression/condition; finalExpression){
            statement
        }
*/

/*
    Will create a loop that will start from 0 and end at 20
    For every iteration of the loop, the value of count will be checked if it is equal/less than 20
    If the value of count is less than or equal to 20, the statement inside of the loop will execute
    The value of count will be incremented by one for each iteration

*/

for (let count = 0; count <= 20; count++) {
  console.log("For: " + count);
}

// [Strings]

let myString = "Carly Rae Jepsen";
// Characters in a string may be counted using the .length property
console.log(myString.length);

// Accessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);

// Will create a loop that will print out the individual letters of the myString variable
for (let x = 0; x < myString.length; x++) {
  console.log(myString[x]);
}

// Create a string named "myName" with a value of your name
let myName = "Hachi";

/*
    Create a loop that will print out the letters of the name individually
*/
for (let i = 0; i < myName.length; i++) {
  //console.log(myName[i].toLowerCase());
  if (
    myName[i].toLowerCase() == "a" ||
    myName[i].toLowerCase() == "i" ||
    myName[i].toLowerCase() == "u" ||
    myName[i].toLowerCase() == "e" ||
    myName[i].toLowerCase() == "o"
  ) {
    console.log(3);
  } else {
    console.log(myName[i]);
  }
}

// [Continue & Break Statements]
/*
    Continue: Allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
    Break: Terminates a current loop once a match has been found
*/

/* 
Create a loop that if the count value is divisible by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
*/

for (let count = 0; count <= 20; count++) {
  if (count % 2 === 0) {
    continue;
  }
  console.log("Continue and Break: " + count);
  if (count > 10) {
    break;
  }
}

let loopName = "Bernardo";
for (let i = 0; i < loopName.length; i++) {
  // If the vowel is equal to a, continue to the next iteration of the loop
  if (loopName[i].toLowerCase() === "a") {
    console.log("Continue to next iteration");
    continue;
  }
  // Current letter printed out based on its index
  console.log(loopName[i]);

  // If the current letter is equal to d, stop the loop
  if (loopName[i].toLowerCase() == "d") {
    break;
  }
}

/* function stringLooper(str) {
  // STEP 6
  let filteredString = ""; // STEP 6

  for (let i = 0; i < str.length; i++) {
    // STEP 6
    let letter = str[i]; // STEP 6

    if (
      letter === "a" ||
      letter === "e" ||
      letter === "i" ||
      letter === "o" ||
      letter === "u"
    ) {
      // STEP 7
      continue; // STEP 7
    } else {
      filteredString += letter; 
    }
  }
  return filteredString; 
} */

/*
let stringVariable = "supercalifragilisticexpialodocious";
console.log(stringVariable);
let filteredString = "";

for (i = 0; i < i < stringVariable.length; i++) {
  if (
    stringVariable[i].toLowerCase == "a" ||
    stringVariable[i].toLowerCase == "i" ||
    stringVariable[i].toLowerCase == "u" ||
    stringVariable[i].toLowerCase == "e" ||
    stringVariable[i].toLowerCase == "o"
  ) {
    continue;
  } else {
    filteredString += stringVariable[i];
  }
}

console.log(filteredString);

*/

/* let stringVariable = "supercalifragilisticexpialidocious";
console.log(stringVariable);
let filteredString = "";

for (i = 0; i < stringVariable.length; i++) {
  if (
    stringVariable[i] == "a" ||
    stringVariable[i] == "e" ||
    stringVariable[i] == "i" ||
    stringVariable[i] == "o" ||
    stringVariable[i] == "u"
  ) {
    continue;
  } else {
    filteredString += stringVariable[i];
  }
}
console.log(filteredString);
*/

//Objective 1

function numberLooper(number) {
  let message = "";
  for (let count = number; count >= 0; count--) {
    if (count <= 50) {
      message = "The current value is at " + count + ". Terminating the loop.";
      break;
    }
    if (count % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
      continue;
    }
    if (count % 5 === 0) {
      console.log(count);
    }
  }
  return message;
}

//Objective 2
let string = "supercalifragilisticexpialidocious";
console.log(string);
let filteredString = "";

//Add code here

// Member 3
for (i = 0; i < string.length; i++) {
  //Member 4
  if (
    string[i] == "a" ||
    string[i] == "e" ||
    string[i] == "i" ||
    string[i] == "o" ||
    string[i] == "u"
  ) {
    continue;
  } else {
    filteredString += string[i];
  }
}
console.log(filteredString);
