let http = require("http");
let port = 4000;

let app = http.createServer((request, response) => {
  /* 
    HTTP requests are differentiated by their endpoint and methods
    HTTP methods tell the server what action it must take, or what kind of response is needed for a request
    With an HTTP method, you can create routes with the same endpoint and a different method
*/
  /* 
    url: localhost:4000/
    method: GET

    url: localhost:4000/
    method: POST
*/

  if (request.url == "/items" && request.method == "GET") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Data received from the database!");
  }

  if (request.url == "/items" && request.method == "POST") {
    response.writeHead(200, { "Content-type": "text/plain" });
    response.end("Data to be sent to the database!");
  }
});

/* 
    Listen method 
    - Can accept two arguments:
        - Port number - assigned to server
        - Callback method/function - when the server is running
*/
app.listen(port, () => console.log(`Server running at localhost:${port}`));
