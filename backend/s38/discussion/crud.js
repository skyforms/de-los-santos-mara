let http = require("http");
let port = 4000;

let directory = [
  {
    name: "Zara Evergreen",
    email: "zara.evergreen@gmail.com",
  },
  {
    name: "Joy Boy",
    email: "joyboy@gear5.com",
  },
];

let app = http.createServer((request, response) => {
  if (request.url == "/users" && request.method == "GET") {
    response.writeHead(200, { "Content-type": "application/json" });
    response.write(JSON.stringify(directory));
    response.end();
    // response.write - used to send data to the client in chunks - partial data without ending the response
    // response.end - finalizes the response, sends final data
  } else if (request.url == "/users" && request.method == "POST") {
    // Route to add a new user - receive input from the client
    // To receive the request body/input from the request, must add a way to receive input - done in two steps

    // requestBody - placeholder to contain data passed from the client
    let requestBody = "";

    // Step 1 - "Data Step"
    // Reads the incoming stream of data from the client for processing, which can then be saved in the requestBody variable

    request.on("data", (data) => {
      console.log(`This is the data received from the client: ${data}`);
      requestBody += data;
    });
    // Step 2 - "End Step"
    // Runs once the request data has been completely sent from the client

    // requestBody is initially in JSON formatting
    // Data received from the client cannot bea dded to the directory array because it is in string format
    // requestBody should be updated with a parsed version of the received JSON format data

    request.on("end", () => {
      console.log(`This is the requestBody: ${requestBody}`);
      console.log(typeof requestBody);
      requestBody = JSON.parse(requestBody);

      let newUser = {
        name: requestBody.name,
        email: requestBody.email,
      };

      console.log(requestBody);
      console.log(typeof requestBody);
      console.log(`This is the email: ${requestBody.email}`);

      directory.push(newUser);
      console.log(`This is the email: ${newUser.email}`);

      response.writeHead(200, { "Content-type": "application/json" });
      // response.write(JSON.stringify(directory));
      response.write(JSON.stringify(newUser.name));
      response.end();
    });
  }
});

app.listen(port, () => console.log(`Server running at localhost:${port}`));
