console.log("Array Traversal!");

// Arrays are used to store multiple related data/values in a single variable
// To declare and create arrays, we use [] also known as "array literals"

let task1 = "Brush Teeth";
let task2 = "Eat Breakfast";

let hobbies = ["Play TOTK", "Read a book", "Listen to music", "Code"];
console.log(typeof hobbies);

/*
Arrays are a special type of object
They have key value pairs
Index number : element
Arrays make it easy to manage and manipulate as sets of data
Method - another term for functions associated with an object, used to execute statements relevant to a specific object

    Syntax:
    let/const arrayName = [elementA, elementB, elementC ...]
*/

// Common array examples
let grades = [98.5, 94.6, 90.8, 88.9];
let computerBrands = [
  "Acer",
  "Asus",
  "Lenovo",
  "HP",
  "Neo",
  "Red Fox",
  "Toshiba",
  "Fujitsu",
];

// Mixed arrays are *not* recommended
let mixedArr = [12, "Asus", null, undefined, ""];
let arraySample = ["Cardo", "One Punch Man", 25000, false];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);

// Mini-Activity
/* 
Create a variable that will store an array of at least 5 of your daily routine on the weekends
Create a variable which will store at least 4 capital cities in the world
Log the variables in the console 
*/

let tasks = [
  "Write in journal",
  "Do yoga",
  "Read a book",
  "Meditate",
  "Go for a walk",
];
let capitalCities = ["Prague", "Seoul", "Tokyo", "Vienna", "Ottawa"];

console.log(tasks);
console.log(capitalCities);

// Alternative way to write arrays
let myTasks = ["Drink html", "Eat JavaScript", "Inhale CSS", "Bake React"];

// Create an array with values from variables
let username1 = "gdragon";
let username2 = "gdino";
let username3 = "ladiesman217";
let username4 = "transformers";
let username5 = "noobmaster68";
let username6 = "gbutiki";

let guildMembers = [
  username1,
  username2,
  username3,
  username4,
  username5,
  username6,
];

console.log(guildMembers);
console.log(myTasks);

// [.length property]
// Allows us to get and set the total number of items or elements in an array
// Gives a number data type

console.log(myTasks.length);
console.log(capitalCities.length);

let blankArr = [];
console.log(blankArr.length);

// Length property can also be used with strings
let fullName = "Hachi Yanagi";
console.log(fullName.length);

// Length property can also set the total number of items in an array
// The array can be shortened simply by updating the length property of an array
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// To delete a specific item in an array, we can employ array methods

// Another example decrementation
capitalCities.length--;
console.log(capitalCities);

// Can we do the same with a string? No.
fullName.length = fullName.length - 1;
console.log(fullName.length);

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

theBeatles[4] = fullName;
console.log(theBeatles);

theBeatles[theBeatles.length] = "Tanggol";
console.log(theBeatles);

// [Read from Arrays]
/*
    Access elements with the use of their index
    Syntax:
    arrayName[index]
*/

console.log(capitalCities[0]); // Accessed the first element of the array

console.log(grades[100]);

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[2]);
console.log(lakersLegends[4]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("Array before assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after-reassignment");
console.log(lakersLegends);

// Mini-Activity 2
let favoriteFoods = ["Tonkatsu", "Adobo", "Pizza", "Lasagna", "Sinigang"];

console.log(favoriteFoods);

favoriteFoods[3] = "Natto";
favoriteFoods[4] = "Jjajangmyun";
console.log(favoriteFoods);

// Mini-Activity 3

function findBlackMamba(index) {
  return lakersLegends[index];
}

let blackMamba = findBlackMamba(0);
console.log(blackMamba);

// Mini-Activity 4
let theTrainers = ["Ash"];
// function addTrainers(name) {
//   theTrainers.push(name);
// }

// console.log(theTrainers);

// addTrainers("Misty");
// addTrainers("Brock");
// console.log(theTrainers);

theTrainers.length++;
console.log(theTrainers);

theTrainers[1] = "Misty";
theTrainers[theTrainers.length] = "Brock";
console.log(theTrainers);

// Access the last element of an array
// Since the firs telement of an array starts at 0, subtracting 1 to the length of the array will offset the value by one
// This allows us to get the last element

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length - 1;

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length - 1]);

// Add items into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockheart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

newArr[newArr.length - 1] = "Aerith Gainsborough";
console.log(newArr);

// Mutator & Non-mutator Methods
/* newArr.pop();
newArr.push();
newArr.shift();
newArr.unshift();
newArr.splice();
newArr.slice;
*/

// [Loop over an array]
for (let index = 0; index < newArr.length; index++) {
  console.log(newArr[index]);
}

let numArr = [5, 12, 30, 48, 40];
for (let index = 0; index < numArr.length; index++) {
  if (numArr[index] % 5 === 0) {
    console.log(numArr[index] + " is divisible by 5!");
  } else {
    console.log(numArr[index] + " is not divisible by 5!");
  }
}

// [Multidimensional Arrays]

let chessBoard = [
  ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
  ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
  ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
  ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
  ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
  ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
  ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
  ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

console.log(chessBoard);

// Access elements of a multidimensional array
console.log(chessBoard[1][4]); // E2
console.log("Pawn moves to: ");
console.log(chessBoard[1][4]); // F2

// A8
console.log(chessBoard[7][0]);

// H6
console.log(chessBoard[5][7]);

// A1
console.log(chessBoard[0][0]);
