console.log("Hello World!");

// Functions
// Parameters & Arguments

/* function printName() {
  let nickname = prompt("Enter your nickname: ");
  console.log("Hi, " + nickname);
}

printName();
 */

function printName() {
  let nickname = prompt("Enter your nickname: ");
  console.log("Hi, " + nickname);
}
printName("Hachi");

let sampleVariable = "Makoto";
printName(sampleVariable);

function checkDivisibilityBy8(num) {
  let remainder = num % 8;
  console.log("The remainder of " + num + " divided by 8 is " + remainder);
  let isDivisibleby8 = remainder === 0;
  console.log("Is" + num + " divisible by 8? ");
  console.log(isDivisibleby8);
}

checkDivisibilityBy8(5136);

/*

    Mini-Activity
    Check the divisibility of a number by 4.

*/

function checkDivisibilityBy4(num) {
  let remainder = num % 4;
  console.log("The remainder of " + num + " divided by 4 is " + remainder);
  let isDivisibleby4 = remainder === 0;
  console.log("Is " + num + " divisible by 4?");
  console.log(isDivisibleby4);
}

checkDivisibilityBy4(56);
checkDivisibilityBy4(95);
checkDivisibilityBy4(444);

// Functions as arguments
// Function parameters can also accept other functions as arguments

function argumentFunction() {
  console.log(
    "This function was passed as an argument before the message was printed."
  );
}

function invokeFunction(argumentFunction) {
  argumentFunction();
}

// Adding and removing the parentheses impacts the output of JS heavily
// When a function is used with (), it denotes invoking or calling a function
// When used without (), it is associated with using the function as an argument to another function
invokeFunction(argumentFunction);

// Using multiple parameters

function createFullName(firstName, middleName, lastName) {
  console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");
createFullName("Cruz", "Dela", "Juan");
createFullName("Juan", "Dela");
createFullName("Juan", "Dela", "Cruz", "III");

// Using variables as arguments

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

/* 
    Mini-Activity

    Create a function called printFriends

    3 parameters: friend1, friend2, friend3
*/

function printFriends(friend1, friend2, friend3) {
  console.log("My 3 friends are: " + friend1 + ", " + friend2 + ", " + friend3);
}

printFriends("Bryan", "Armand", "Jac");

// Return Statement

function returnFullName(firstName, middleName, lastName) {
  return firstName + " " + middleName + " " + lastName;
  console.log("This will not be printed!");
}

let completeName = returnFullName("Monkey", "D", "Luffy");

console.log(completeName + " is my best friend!");

/*

    Mini-Activity

    Create a funciton that will calculate the area of a square.
    Create a function that will add 3 numbers.
    Create a function that will check if the number is equal to 100.

*/

function calculateSquareArea(sideLength) {
  return sideLength * sideLength;
}

let squareArea = calculateSquareArea(4);
console.log("The area of a square with a length of 4 is: " + squareArea);

function addThreeNumbers(num1, num2, num3) {
  return num1 + num2 + num3;
}

let sumOfThreeNumbers = addThreeNumbers(1, 2, 3);
console.log("The sum of three numbers is: " + sumOfThreeNumbers);

function checkNumberEqualTo100(number) {
  return number === 100;
}

let isNumberEqualto100 = checkNumberEqualTo100(84);
console.log("Is this 100? " + isNumberEqualto100);
