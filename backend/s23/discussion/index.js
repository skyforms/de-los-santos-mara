console.log("Hello, World!");

// If, else if, and else statements

let numG = -1;

// if statement
// Executes a statement if a specified condition is met

if (numG < 0) {
  console.log("Hello! The condition in the if statement is true.");
}

let numH = 1;

// else if clause
// execute a statement if previous condition is false and specified condition is true
// the else if clause is optional and can be added to capture additional conditions to change the flow of a program

if (numG > 0) {
  console.log("Hello!");
} else if (numH > 0) {
  console.log(
    "This will be the log if the else if condition is true and the if condition is true."
  );
}

// else statement
// executes a statement if all other conditions are false
// the "else" statement is OPTIONAL and can be added to capture any other result to change the flow of our program

if (numG > 0) {
  console.log("I'm enchanted to meet you!");
} else if ((numH = 0)) {
  console.log("It's me, hi...");
} else {
  console.log("Hello from the other side!");
}

// if, else if, and else statements with functions

let message = "No message!";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
  if (windSpeed < 30) {
    return "Not a typhoon yet.";
  } else if (windSpeed <= 61) {
    return "Tropical Depression detected!";
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return "Tropical Storm detected!";
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return "Severe Tropical Storm detected!";
  } else {
    return "Typhoon Detected!";
  }
}

// && and
// || or
// ! not

message = determineTyphoonIntensity(65);
console.log(message);

if (message == "Tropical Storm detected!") {
  console.warn(message);
}

// Truthy and falsy values

/*
    In JS - every value is "truthy" when encountered in a Boolean context, except:
    1. False
    2. 0
    3. -0
    4. ""
    5. null
    6. undefined
    7. NaN (not a number)
*/

// Truthy examples
if (true) {
  console.log("This is truthy.");
}
if (1) {
  console.log("This is truthy.");
}
if ([]) {
  console.log("This is truthy.");
}

// Falsy examples
if (false) {
  console.log("This will not log in the console.");
}

if (0) {
  console.log("This will not log in the console.");
}

if (undefined) {
  console.log("This will not log in the console.");
}

if ("Hachi") {
  console.log("This will log in the console.");
}

// [conditional (ternary) operator]
/* 
    Takes in 3 operands:
    1. Condition
    2. Expression to execute if the condition is truthy
    3. Expression to execute if the condition is falsy

    Syntax:
        (condition) ? ifTrue : ifFalse
*/

// single statement execution
let ternaryResult = 1 < 18 ? true : false;
console.log("Result of ternary operator: " + ternaryResult);

// multiple statement execution

let name;

function isOfLegalAge() {
  name = "John";
  return "You are of the legal age limit!";
}

function isUnderAge() {
  name = "Jane";
  return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = age > 18 ? isOfLegalAge() : isUnderAge();

console.log(
  "Result of ternary operator in functions: " + legalAge + ", " + name
);

//[switch statement]
// Can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an expected input

/* 
    Syntax
    switch (expression){
        case value:
            statement;
            break;
        default:
            statement;
            break;
    }
*/

let day = prompt("What day of the week is it?").toLowerCase();
console.log(day);

/*
    Mini-Activity
    Create cases for Thursday, Friday, Saturday, and Sunday
*/

switch (day) {
  case "monday":
    console.log("The color of the day is blue!");
    break;
  case "tuesday":
    console.log("The color of the day is yellow!");
    break;
  case "wednesday":
    console.log("The color of the day is red!");
    break;
  case "thursday":
    console.log("The color of the day is green!");
    break;
  case "friday":
    console.log("The color of the day is gold!");
    break;
  case "saturday":
    console.log("The color of the day is brown!");
    break;
  case "sunday":
    console.log("The color of the day is purple!");
    break;
  default:
    console.log("Please input a valid day.");
    break;
}

//[try-catch-finally statement]
// Commonly used for error handling

function showIntensityAlert(windSpeed) {
  try {
    // Within the try block, we can attempt to execute a codde
    alerat(determineTyphoonIntensity(windSpeed));
  } catch (error) {
    console.log(typeof error);
    // "error.message" is used to access the information relating to the error object
    console.warn(error.message);
  } finally {
    alert("Intensity updates will show new alert");
  }
}
showIntensityAlert(56);
console.log("Hi");
