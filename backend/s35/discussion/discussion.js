// MongoDB Query Operators & Field Projection

// [Comparison Query Operator]
/* 
    $gt/$gte - > Operator
        - Allows us to find documents that have field number values greater than or equal to the specified value
   
    Syntax: 
        db.collectionName.find({field: {$gt: value}})
        db.collectionName.find({field: {$gte: value}})
*/

db.users.find({ age: { $gt: 50 } });
db.users.find({ age: { $gte: 50 } });

/*
    $lt/$lte - < Operator
        - Allows us to find documents that have field number values less than or equal to a specified value
    
    Syntax: 
        db.collectionName.find({field: {$lt: value}})
        db.collectionName.find({field: {$lte: value}})
        
*/

db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 50 } });

/* 
    $ne Operator
        - Allows us to find documents that have field number values NOT EQUAL to a specified value

    Syntax: 
        db.collectionName.find({field: {$ne: value}})
*/

db.users.find({ age: { $ne: 65 } });
db.users.find({ age: { $ne: 82 } });

/* 
    $in operator
        - Allows us to find documents with specific match criteria in one field using different values

    Syntax:
        db.collectionname.find({field: {$in: value}}) 
*/

db.users.find({ lastName: { $in: ["Hawking", "Doe"] } });
db.users.find({ courses: { $in: ["HTML", "React"] } });

// [Logical Query Operators]

/* 
    $Or
        - Allows us to find documents that match a single value from multiple provided search criteria

    Syntax:
        db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}]})
*/

db.users.find({ $or: [{ firstName: "Neil" }, { age: 25 }] });
db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 30 } }] });

/* 
    %And
        - Allows us to find documents matching multiple criteria in a single field

    Syntax:
        db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]})
*/

db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 } }] });

// [Field Projection]
/* 
    Operations that retrieve documents are commonly used; by default, MongoDB queries return the whole document as a response

    Using the following operations, fields can now be included/excluded from the response
*/

/* 
    Inclusion
        - Allows us to include/add specific fields when retrieving documents
        - The value provided is 1, to denote that the field is being included
    
    Syntax:
        db.collectionName.find({criteria}, {field: 1})
*/

db.users.find(
  { firstName: "Jane" },

  {
    firstName: 1,
    lastName: 1,
    contact: 1,
  }
);

/* 
    Exclusion
        - Allows us to exclude/remove specific fields when retrieving documents
        - The value 0 denotes that the field is being excluded

    Syntax:
        db.users.find({criteria}, {field: 0})

*/

db.users.find(
  {
    firstName: "Jane",
  },
  {
    contact: 0,
    department: 0,
  }
);

/* 
    Suppressing the ID Field 
        - Allows us to exclude the :_id" field when we are retrieving documents
        - When using field projection, field inclusion and exclusion may not be used at the same time
        - Exception: "_id" field
    
    Syntax:
        db.collectionName.find({criteria}, {_id: 0})
*/

db.users.find(
  { firstName: "Jane" },
  { firstName: 1, lastName: 1, contact: 1, _id: 0 }
);

// [Evaluation Query Operators]
/* 
    $regex Operator
        - Allows us to find documents that match a specific string pattern using regular expressions
    
    Syntax:
        db.users.find({field: {$regex: "pattern"}, $options: "optionvalue"})
*/

// Case Sensitive Query
db.users.find({ firstName: { $regex: "N" } });

// Case Insensitive Query
db.users.find({ firstName: { $regex: "j", $options: "i" } });
// i = case insensitivity

// Activity

// S35 - MongoDB Query Operators and Field Projection:

/*

    Sample solution:

    return async function findName(db) {
        await (db.collectionName.find({
            $and: [
            	{field1: "value1"},
             	{field2: "value2"}
            ]
        }));
        
    }

Note: 
	- Do note change the functionName or modify the exports
	- Delete all the comments before pushing.

*/

// 1. Find users with letter s in their first name or d in their last name.
async function findName(db) {
  return await (
    db.users.find({
    $or: [
      { firstName: { $regex: "s", $options: "i" } },
      { lastName: { $regex: "d", $options: "i" } },
    ],
  })
  );
}

// 2. Find users who are from the HR department and their age is greater than or equal to 70.
async function findDeptAge(db) {
  return await (
    db.users.find({ department: "HR", age: { $gte: 70 } })
  );
}

// 3. Find users with the letter e in their last name and has an age of less than or equal to 30.
async function findNameAge(db) {
  return await (
    db.users.find({
    $and: [
        { lastName: { $regex: "e", $options: "i" } }, 
        { age: { $lte: 30 } }
    ]
  })
  );
}
