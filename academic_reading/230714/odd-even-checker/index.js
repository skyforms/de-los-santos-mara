let oddEvenChecker = () => {
  for (let i = 1; i <= 50; i++) {
    if (i % 2 === 0) {
      console.log(`The number ${i} is even`);
    } else {
      console.log(`The number ${i} is odd`);
    }
  }
};

oddEvenChecker();
