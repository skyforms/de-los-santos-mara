let grade = parseInt(prompt("Enter your grade:"));

let getGrade = () => {
  if (grade <= 80) {
    return "You are a novice.";
  }
  if (grade >= 81 && grade <= 90) {
    return "You are an apprentice.";
  }
  if (grade >= 91 && grade <= 100) {
    return "You are a master!";
  }
};

getGrade();
console.log(`Your grade is: ${grade}`);
console.log(getGrade());
