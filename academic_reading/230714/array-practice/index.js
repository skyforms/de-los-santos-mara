console.log("Array Practice");

// Array Methods

// Join
let color = ["Blue", "Red", "Yellow", "Orange", "Green", "Purple"];

function joinArray(arr) {
  return arr.join(", ");
}

console.log("Array:");
console.log(color);
console.log("Joined Array:");
console.log(joinArray(color));

// Unshift
let planets = ["Venus", "Mars", "Earth"];

function unshiftArray(arr, planet) {
  arr.unshift(planet);
}

console.log("Array:");
console.log(planets);

unshiftArray(planets, "Jupiter");
console.log("Array w/ Addition:");
console.log(planets);

// Pop

let favSingers = ["Brandon Boyd", "Aimer", "Baek Yerin", "IU", "Ebe Dancel"];

function popArray(arr) {
  arr.pop(arr);
}

console.log("Array:");
console.log(favSingers);

popArray(favSingers);
console.log("Array w/ Removed Element:");
console.log(favSingers);
